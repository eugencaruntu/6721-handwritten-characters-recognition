import os
import sys

from sklearn import metrics
from sklearn.externals import joblib

from Data import Data


def write_csv(prediction, file_path):
    with open(file_path, 'w') as file:
        for i in range(len(prediction)):
            file.write('%d,%d\n' % (i + 1, prediction[i]))


root_folder = '/home/eugen/PycharmProjects/6721_Project2_29077103/Models'
out_folder = '/home/eugen/PycharmProjects/6721_Project2_29077103/Out'

data_folder = '/home/eugen/PycharmProjects/6721_Project2_29077103/data'
data_sets = ['ds1', 'ds2']
validation_types = ['Val', 'Test']

for ds in data_sets:
    models_folder = root_folder + '/' + ds
    model_names = [os.path.splitext(x)[0] for x in os.listdir(models_folder)]
    for val_type in validation_types:
        d = Data(data_folder, ds, val_type)
        results = {}
        for model_file in model_names:
            model = joblib.load(models_folder + '/' + model_file + '.joblib')
            prediction = model.predict(d.X_validation)
            print(metrics.accuracy_score(d.y_validation, prediction))
            write_csv(prediction, out_folder + '/' + ds + val_type + '-' + model_file + '.csv')
'''
            # Comment out for demo due to limited dependencies restrictions
            if val_type == 'Val':
                results[model_file] = metrics.accuracy_score(d.y_validation, prediction)

            # Predict a specific line
            predict_specific_line_index(ds + val_type, d.mapping, 11, model, d.X_validation)

        if val_type == 'Val':
            plot_accuracy(ds, results, root_folder)
        # End comment out
'''

sys.exit(0)
