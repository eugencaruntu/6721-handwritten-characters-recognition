import os

from sklearn import model_selection
from sklearn.ensemble import RandomForestClassifier
from sklearn.externals import joblib
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import GridSearchCV
from sklearn.naive_bayes import BernoulliNB, MultinomialNB, GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier, ExtraTreeClassifier

from Data import Data
from Util import kfold_learning_curve, report_performance, kfold_cross_validation, plot_parameters, class_representation


class Classifiers:
    def __init__(self, data_folder, ds):
        data_set = ds
        d = Data(data_folder, data_set, 'Val')
        X_train = d.X_train
        y_train = d.y_train
        X_validation = d.X_validation
        y_validation = d.y_validation
        X_combined = d.X_combined
        y_combined = d.y_combined
        mapping = d.mapping

        # SET THE WORKING FOLDER BASED ON DATA SET
        working_folder = 'Exploring ' + data_set
        os.makedirs(working_folder, exist_ok=True)
        os.chdir(working_folder)
        os.makedirs('_parameter selection', exist_ok=True)

        # Plot the class representation in data set
        class_representation(data_set, y_train)

        # SET THE K FOLD
        n_splits = 5
        kfold = model_selection.KFold(n_splits=n_splits, random_state=42)

        # EXPLORE BEST HYPER-PARAMETERS BY CROSS VALIDATION WITH ACCURACY, PRECISION, RECALL
        parameter_selection = {
            MultinomialNB(): [{'fit_prior': [False, True], 'alpha': [0.01, 0.05, 0.5, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 15, 20]}],
            BernoulliNB(): [{'fit_prior': [False, True], 'alpha': [0.0001, 0.001, 0.01, 0.05, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1, 2]}],
            GaussianNB(): [{'var_smoothing': [0.0001, 0.001, 0.01, 0.05, 0.1, 0.25, 0.5, 0.75, 1, 1.5, 2]}],
            DecisionTreeClassifier(): [{'criterion': ['entropy', 'gini'], 'max_depth': [50, 100, 200, 300], 'max_leaf_nodes': [700, 900], 'max_features': [100, 500, 800]}],
            ExtraTreeClassifier(): [{'criterion': ['entropy', 'gini'], 'max_depth': [50, 100, 200, 300], 'max_leaf_nodes': [700, 900], 'max_features': [100, 500, 800]}],
            RandomForestClassifier(random_state=42, n_jobs=-1, max_depth=50): [{'max_features': [200, 500], 'n_estimators': [300, 500, 700]}],
            MLPClassifier(random_state=42, max_iter=1000, learning_rate='invscaling'): [{'hidden_layer_sizes': [(30, 20, 10, 5), (200, 50), (200, 100, 50), (300, 200, 100, 50)]}],
            SVC(random_state=42): [{'kernel': ['rbf'], 'gamma': [1e-3, 1e-4], 'C': [1, 10, 100, 1000]},
                                   {'kernel': ['linear'], 'C': [1, 10, 100, 1000]}],
            LogisticRegression(random_state=42, n_jobs=-1, multi_class='auto'): [{'solver': ['newton-cg', 'sag', 'saga']}],
            KNeighborsClassifier(n_jobs=-1, leaf_size=30): [{'n_neighbors': [1, 3, 5]}],
        }

        params = ['accuracy', 'precision_macro', 'recall_macro']

        # the collection of tuned models obtained after grid search
        tuned_models = []

        for model, parameter in parameter_selection.items():
            model_name = model.__class__.__name__
            best = {}
            means = []
            labels = []
            for param in params:
                tuned_model = GridSearchCV(model, parameter, cv=kfold, scoring=param)
                tuned_model.fit(X_combined, y_combined)
                means.append(tuned_model.cv_results_['mean_test_score'])
                best[param] = tuned_model.best_params_
                if param == 'accuracy':  # collect the best model optimized for accuracy
                    tuned_models.append(tuned_model.best_estimator_)
                labels = [str(v) for v in tuned_model.cv_results_['params']]

            plot_parameters(data_set, model_name, labels, means, best)

        # COMPARE THE BEST MODELS AS FOUND EARLIER
        kfold_cross_validation(data_set, tuned_models, X_combined, y_combined, kfold)

        # TRAIN, PREDICT, SAVE RESULTS AND PERFORMANCE, LEARNING CURVE
        for model in tuned_models:
            model_name = model.__class__.__name__
            os.makedirs(model_name, exist_ok=True)

            # Learning curve
            kfold_learning_curve(data_set, model, X_combined, y_combined, kfold)

            # Train, save, predict based on train set only
            model.fit(X_train, y_train)
            # export_graphviz(model, out_file=model_name + '.dot', max_depth=5) # for tree discussion only (focus on entropy, info. gain)
            joblib.dump(model, model_name + '/' + model_name + '.joblib')
            prediction = model.predict(X_validation)
            report_performance(data_set, X_validation, y_validation, prediction, model_name)

            # Train on complete set and save
            model.fit(X_combined, y_combined)
            joblib.dump(model, model_name + '/' + model_name + '_trained on complete set.joblib')
