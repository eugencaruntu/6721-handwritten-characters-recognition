import sys

from Classifiers import Classifiers

data_folder = '/home/eugen/PycharmProjects/6721_Project2_29077103/data'
data_sets = ['ds1', 'ds2']

for ds in data_sets:
    experiment = Classifiers(data_folder, ds)
    print('Finished experiments for ' + ds)

sys.exit(0)
