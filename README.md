# 6721 AI ML

To load and predict on test set, first change the path for the absolute path to the data folder inside `Predict.py`.
The expected structure of data folders must be:

```
data_folder
 |
 +-- ds1
 |  | 
 |  +-- ds1Info.csv
 |  +-- ds1Test.csv
 |  +-- ds1Train.csv
 |  +-- ds1Validation.csv
 |
 +-- ds2
 |  |
 |  +-- ds2Info.csv
 |  +-- ds2Test.csv
 |  +-- ds2Train.csv
 |  +-- ds2Validation.csv
```
To execute the script run `Predict.py`. It will load the saved classifiers ffrom their respective *Model* folder and run them against the Val and Test to generate predictions as indicated in `validation_types = ['Val', 'Test']`
The output can be found in *Out* folder. Some features are currently commented out in `Predict.py` as they would need additional dependecies.

The `Data.py` is handling data splits for features/labels and different data sets. It has no additional dependencies.

More features are present in `Util.py` and  `Classifiers.py` once their dependencies are resolved. Their driver script is `Experimentation.py`