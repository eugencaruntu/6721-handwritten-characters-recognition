class Data:
    def __init__(self, data_folder, ds, val_test):
        folder = data_folder + '/' + ds + '/'
        train_data = read_csv(folder + ds + 'Train.csv')
        validation_data = read_csv(folder + ds + val_test + '.csv')

        self.mapping = folder + '/' + ds + 'Info.csv'

        self.X_train = [d[:-1] for d in train_data]
        self.y_train = [d[-1] for d in train_data]

        # The type of validation: labeled validaion or test without labels
        if val_test == 'Test':
            self.X_validation = validation_data
        if val_test == 'Val':  # the test set has no labels, so we do this for validation set only
            self.X_validation = [d[:-1] for d in validation_data]
            self.y_validation = [d[-1] for d in validation_data]
            # Combined data is used only for Cross Validation scoring while tuning the models
            combined_data = train_data + validation_data
            self.X_combined = [d[:-1] for d in combined_data]
            self.y_combined = [d[-1] for d in combined_data]


def read_csv(file_path):
    with open(file_path, 'r') as file:
        lines = (line.rstrip() for line in file.read().split('\n'))  # [1:] if we have headers
        data = [line.split(',') for line in lines if line]
    return [[int(element) for element in row] for row in data]

# SPLIT THE DATA
# split_test_size = 0.2
# X_train, X_validation, y_train, y_validation = train_test_split(X, y, test_size=split_test_size, random_state=42)
