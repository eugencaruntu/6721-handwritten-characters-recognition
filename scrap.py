# Tune hyper-parameters for a particular model. Make sure to pass the correct interval (start, end, step)
import os
import pickle

from sklearn.externals import joblib
from sklearn.model_selection import cross_val_score
from sklearn.neural_network import MLPClassifier
from sklearn.tree import ExtraTreeClassifier, DecisionTreeClassifier

from Util import plot_parameters


def explore_parameters(model, start, end, step):
    # Explore parameters for each model
    param_range = np.arange(start, end, step)
    model_name = model.__class__.__name__
    os.makedirs(model_name, exist_ok=True)
    param_scores = []
    for value in param_range:
        # model.alpha = value             # for Bernoulli and Multinomial
        model.var_smoothing = value  # for Gaussian
        # model.max_depth = 200           # for Decision Tree # depth is not relevant, AND RandomForestClassifier
        # model.max_leaf_nodes = 700      # for Decision Tree
        # model.max_features = value      # for Decision Tree AND RandomForestClassifier
        # model.leaf_size = value         # for KNeighborsClassifier THE n_neighbor has no impact thus we try leaf_size
        # model.n_estimators = value      # for RandomForestClassifier
        scores = cross_val_score(model, X_combined, y_combined, cv=kfold, scoring='accuracy')
        param_scores.append(scores.mean())  # append the mean of the k fold experiments
    plot_parameters(data_set, model_name, param_range, param_scores)


# explore_parameters(GaussianNB(), 0.0001, 1, 0.05)

# The selected models and their already tuned parameters
models = [
    ExtraTreeClassifier(),
    ExtraTreeClassifier(criterion='gini', max_depth=10, max_features=300),
    ExtraTreeClassifier(criterion='entropy', max_depth=10, max_features=300),
    MLPClassifier(activation="relu", max_iter=1000, learning_rate="invscaling"),
    MLPClassifier(activation="relu", max_iter=1000, learning_rate="adaptive"),
    MLPClassifier(activation="relu", max_iter=1000, learning_rate="constant"),
    DecisionTreeClassifier(),
    DecisionTreeClassifier(criterion='gini', max_depth=10, max_features=300),
    DecisionTreeClassifier(criterion='entropy', max_depth=10, max_features=300),
]
# models
# models = [
#         BernoulliNB(alpha=0.0001),
#           MultinomialNB(alpha=6.5),
#           GaussianNB(var_smoothing=0.1),
#           DecisionTreeClassifier(criterion='entropy', max_depth=200, max_leaf_nodes=700, max_features=300),
#           LogisticRegression(random_state=42, n_jobs=-1, solver='newton-cg', multi_class='auto'),
#           RandomForestClassifier(max_features=400, n_estimators=125, max_depth=100, random_state=42, n_jobs=-1),
#           KNeighborsClassifier(leaf_size=3),
#           SVC(kernel='linear', C=1, random_state=42)
#           ]

# Convert Pickle to Joblib
def convert_model_format(directory):
    for root, dirs, files in os.walk(directory):
        for file in files:
            if file.endswith('.model'):
                file_path = os.path.abspath(root) + '/' + file
                model = pickle.load(open(file_path, 'rb'))
                joblib.dump(model, file_path + '.joblib')
