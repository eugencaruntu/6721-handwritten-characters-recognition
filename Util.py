import numpy as np
from matplotlib import pyplot as plt
from sklearn import metrics, model_selection


def map_to_character(file_path, prediction):
    with open(file_path, 'r') as file:
        data = [line.split(',') for line in file.read().split('\n')][1:]
    map_data = dict((d[0], str(d[1:])) for d in data)
    return map_data[str(prediction[0])]


def show_predicted_character(ds, model_name, file_path, c, prediction):
    c = np.array(c)
    c.shape = (32, 32)
    plt.imshow(255 - c, cmap='gray')
    plt.title('Classified as ' + map_to_character(file_path, prediction) + ' \nby ' + model_name, loc='left', fontweight='bold')
    plt.savefig(ds + '_' + model_name + '_sample', bbox_inches='tight', dpi=300)
    plt.show()


def write_csv(prediction, file_path):
    with open(file_path, 'w') as file:
        for i in range(len(prediction)):
            file.write('%d,%d\n' % (i + 1, prediction[i]))


def write_string(string, file_name):
    with open(file_name, 'w') as text_file:
        text_file.write(string)


def plot_prediction(data_set, y, prediction, model_name):
    acc = '\nAccuracy: {:.2f}%.'.format(100 * metrics.accuracy_score(y, prediction))
    plt.scatter(y, prediction, c=y)
    plt.title(model_name + ' (' + data_set + ')' + acc, loc='left', fontweight='bold')
    plt.xlabel('Expected')
    plt.ylabel('Predicted')
    plt.colorbar()
    plt.tight_layout()
    plt.savefig(model_name + '/Prediction', bbox_inches='tight', dpi=300)
    plt.show()


def plot_cross_validation_scores(data_set, results, model_names):
    plt.boxplot(results, labels=model_names)
    plt.title('Comparing classifiers (' + data_set + ')', loc='left', fontweight='bold')
    plt.xlabel('Models')
    plt.ylabel('Cross-Validated Accuracy')
    plt.xticks(rotation=90)
    plt.tight_layout()
    plt.savefig('Comparing Classifiers', bbox_inches='tight', dpi=300)
    plt.show()


def plot_accuracy(data_set, results, folder):
    plt.plot(*zip(*sorted(results.items(), key=lambda kv: kv[1])), marker="X")
    plt.title('Comparing trained classifiers on data set ' + data_set, loc='left', fontweight='bold')
    plt.xlabel('Models')
    plt.ylabel('Accuracy')
    plt.xticks(rotation=90)
    plt.tight_layout()
    plt.savefig(folder + '/Comparing Trained Classifiers for ' + data_set, bbox_inches='tight', dpi=300)
    plt.show()


def report_performance(data_set, X, y, prediction, model_name):
    plot_prediction(data_set, y, prediction, model_name)
    print("\nAccuracy: {:.2f}%. Total data size: {}. Poor quality data: {}.".format(
        100 * metrics.accuracy_score(y, prediction), len(X), (y != prediction).sum()))
    np.savetxt(model_name + '/Confusion Matrix.txt', metrics.confusion_matrix(y, prediction), fmt='%d')
    write_string(metrics.classification_report(y, prediction), model_name + '/Clasification Report.txt')


def kfold_cross_validation(data_set, models, X, y, kfold):
    results = []
    model_names = []
    for model in models:
        model_name = model.__class__.__name__
        cv_results = model_selection.cross_val_score(model, X, y, cv=kfold, scoring='accuracy')
        results.append(cv_results)
        model_names.append(model_name)
        write_string(cv_results.mean() + '[' + cv_results.std() + ']', 'CV Accuracy ' + model_name + '.txt')
    plot_cross_validation_scores(data_set, results, model_names)


def plot_parameters(data_set, model_name, param_range, values, best):
    plt.plot(param_range, values[0], label='accuracy: ' + str(best['accuracy']))
    plt.plot(param_range, values[1], label='precision: ' + str(best['precision_macro']))
    plt.plot(param_range, values[2], label='recall: ' + str(best['recall_macro']))
    plt.title(model_name + ' (' + data_set + ')', loc='left', fontweight='bold')
    plt.xlabel('Parameter Value')
    plt.ylabel('Cross-Validation Score')
    plt.xticks(rotation=90)
    plt.legend(bbox_to_anchor=(0.5, 1), loc="lower left")
    plt.tight_layout()
    plt.savefig('_parameter selection/' + model_name + ' (tuned parameters)', bbox_inches='tight', dpi=300)
    plt.show()


# http://scikit-learn.org/stable/auto_examples/model_selection/plot_learning_curve.html
def kfold_learning_curve(data_set, model, X, y, kfold):
    model_name = model.__class__.__name__
    train_sizes, train_scores, test_scores = model_selection.learning_curve(model, X, y, cv=kfold, n_jobs=-1)
    plt.plot(train_sizes, train_scores.mean(axis=1), '--', label='Training Score')
    plt.plot(train_sizes, test_scores.mean(axis=1), '-', label='Cross-Validation Score')
    plt.title('Learning Curve for ' + model_name + ' (' + data_set + ')', loc='left', fontweight='bold')
    plt.xlabel('Training set size')
    plt.ylabel('Accuracy')
    plt.legend()
    plt.tight_layout()
    plt.savefig(model_name + '/Learning Curve', bbox_inches='tight', dpi=300)
    plt.show()


def predict_specific_line_index(ds, mapping, idx, model, test):
    line = test[idx]
    show_predicted_character(ds, model.__class__.__name__, mapping, line, model.predict([line]))


def class_representation(data_set, data):
    # print(np.bincount(data))
    classes = np.unique(data)
    plt.hist(data, rwidth=0.75, bins=range(classes.size + 1))
    plt.title('Class representation (' + data_set + ')', loc='left', fontweight='bold')
    plt.xlabel('Classes')
    plt.ylabel('Instances')
    plt.xticks(classes)
    ticklabels = [classes[i] if i % 2 == 0 else "" for i in range(len(classes))]
    plt.gca().set_xticklabels(ticklabels)
    plt.tight_layout()
    plt.savefig('Class Representation', bbox_inches='tight', dpi=300)
    plt.show()
